localStorage.clear();

/* AJAX */
async function sendRequest(method = '', url = '', data = {}, id = '', visit = '') {
    return await axios({
        method: method,
        url: url,
        data: data,
    })
}

/* ModalWindow */
class ModalWindowVersion2 {

    constructor(id, title) {
        this._modalId = id;
        this._title = title;
        this._modal = null;
    }

    render(container) {
        this._modal = document.createElement('div');
        this._modal.id = this._modalId;

        this._modal.innerHTML = `
            <div class="modal modal-window fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
    
                        <div class="modal-header">
                            <h5 class="modal-title">${this._title}</h5>
                            <button id="modalClose" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                       
                        <div class="modal-body"></div>
                        
                        <div id="modalFooter" class="modal-footer"></div>                
                    </div>
                </div>
            </div>`

        container.appendChild(this._modal);
    }
}

const loginServiceBtn = document.getElementById('loginBtn');
loginServiceBtn.addEventListener('click', function () {

    loginServiceBtn.disabled = true;

    const loginModalWindow = new ModalWindowVersion2('authorizeServiceWindow', 'Authorization');
    loginModalWindow.render(document.body);

    const modalWindow = document.querySelector('.modal');
    modalWindow.setAttribute('id', 'modal-registration');

    const modalAuthorizationForm = document.querySelector('.modal-body');
    modalAuthorizationForm.innerHTML = `
            <form id="loginForm">
                <div class="form-group"
                    <label for="loginEmail">Email address</label>
                    <input type="email" name="email" required class="form-control" id="loginEmail" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="loginPassword">Password</label>
                    <input type="password" name="password" required class="form-control" id="loginPassword" placeholder="Password">
                    <small id="passwordHelp" class="form-text text-muted">We'll never share your password with anyone else.</small>
                </div>

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="authorizeSubmitBtn" class="btn btn-primary">Confirm</button>
            </form>
    `;
    const authorizeForm = document.getElementById('loginForm');
    const authorizeBtn = document.getElementById('authorizeSubmitBtn');

    loginServiceBtn.disabled = false;

    /* AJAX POST defaultRequest */
    authorizeForm.addEventListener('submit', async function (e) {
        e.preventDefault();

        const modalLoginEmail = document.getElementById('loginEmail').value;
        const modalLoginPassword = document.getElementById('loginPassword').value;
        const modalFooter = document.querySelector('.modal-footer');

        const postAuthorizeData = {
            email: modalLoginEmail,
            password: modalLoginPassword,
        }

        sendRequest('POST', 'https://ajax.test-danit.com/api/cards/login', postAuthorizeData)
            .then((response) => {
                const {data} = response;
                localStorage.setItem('authorizeToken', data);
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('authorizeToken');
                loginServiceBtn.remove();

                const visitBtn = document.getElementById('visitBtn');
                visitBtn.style.display = 'block';

                if (response.status >= 200 && response.status < 300) {
                    modalFooter.innerHTML = `
                        <div class="alert alert-success" role="alert">
                            Request is success. Thank you :)
                        </div>
                    `

                    const filterBtn = document.getElementById('cardsFilterBtn');
                    filterBtn.disabled = false;
                }
                sendRequest('GET', 'https://ajax.test-danit.com/api/cards')
                    .then((response) => {
                        const container = document.querySelector('.card-container');

                        response.data.forEach(card => {
                            const visitCard = new VisitDoctor(card);
                            visitCard.renderCards(container, card.id);
                        });

                        authorizeBtn.disabled = true;
                        const filterError = document.getElementById('filterError');
                        filterError !== null ? filterError.remove() : '';
                    })
            })
            .catch((error) => {
                const modalFooter = document.querySelector('.modal-footer');

                modalFooter.innerHTML = `
                        <div class="alert alert-danger" role="alert">
                                ${error}. Please check your email, password and qwerty language
                        </div>`
            })
            .then(() => {
                const modalWindow = document.querySelector('.modal');
                modalWindow.removeAttribute('id');
            })
    });
})

const createVisitBtn = document.getElementById('visitBtn');
createVisitBtn.addEventListener('click', function () {

    const loginModal = document.querySelectorAll('#authorizeServiceWindow');
    for (const el of loginModal) {
        el.remove();
    }

    const visitModal = document.querySelectorAll('#modalVisit');
    if (visitModal !== null) {
        visitModal.forEach((el) => {
            el.remove()
        });
    }

    createVisitBtn.disabled = true;

    const modalCreateVisit = new ModalWindowVersion2('modalVisit', 'Visit info: ');
    modalCreateVisit.render(document.body);
    const modalWindow = document.querySelector('.modal-window');
    modalWindow.setAttribute('id', 'modal-visit');

    createVisitBtn.disabled = false;

    const modalBody = document.querySelector('.modal-body');
    Form.chooseDoctor(modalBody);
});
