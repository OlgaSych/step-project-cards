class Input {
    constructor() {}

    createInput() {
        const input = document.createElement('input');
        input.required = true;
        input.classList.add('form-control', 'mb-2', 'field-data');
        return input;
    }
}
