class Select {
    constructor(id, text, properties) {
        this.id = id;
        this.text = text;
        this.properties = properties;
    }
    createSelect() {
        const select = document.createElement('select');
        select.id = this.id;
        select.classList.add('form-control', 'field-data');
        select.insertAdjacentHTML('beforeend', `<option selected disabled>${this.text}</option>`);
        for (let i = 0; i <= this.properties.length - 1; i++) {
            select.insertAdjacentHTML('beforeend', `<option value="${this.properties[i]}">${this.properties[i]}</option>`)
        }
        return select;
    }
}
